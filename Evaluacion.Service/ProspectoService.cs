﻿using Evaluacion.Core.Models;
using Evaluacion.Core.Repositories;
using Evaluacion.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Evaluacion.Service
{
    public class ProspectoService : IProspectoService
    {
        private IProspectoRepository _prospectoReporitory;

        public ProspectoService(IProspectoRepository _prospectoReporitory) 
        {
            this._prospectoReporitory = _prospectoReporitory;
        }

        public async Task AutorizacionProspecto(int estatus, int idprospecto)
        {
            await this._prospectoReporitory.AutorizacionProspecto(estatus, idprospecto);
        }

        public async Task CreateDocumento(Documento documento)
        {
            await this._prospectoReporitory.CreateDocumento(documento);
        }

        public async Task CreateMotivoRechazo(RechazoProspecto motivoRechazo)
        {
            await this._prospectoReporitory.CreateMotivoRechazo(motivoRechazo);
        }

        public async Task<int> CreateProspecto(Prospecto prospecto)
        {
            return await this._prospectoReporitory.CreateProspecto(prospecto);
            
        }

        public async Task<IList<Documento>> GetDocumentoAllByIdProspecto(int idprospecto)
        {
            return await this._prospectoReporitory.GetDocumentoAllByIdProspecto(idprospecto);
        }

        public async Task<IList<Prospecto>> GetProspectoAll()
        {
            return await this._prospectoReporitory.GetProspectoAll();
        }

        public async Task<RechazoProspecto> GetRechazoByIdProspecto(int idprospecto)
        {
            return await this._prospectoReporitory.GetRechazoByIdProspecto(idprospecto);
        }
    }
}
