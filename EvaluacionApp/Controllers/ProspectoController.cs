﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Evaluacion.Core.Models;
using Evaluacion.Core.Services;
using EvaluacionApp.helper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EvaluacionApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProspectoController : ControllerBase
    {
        private IProspectoService _prospectoService;
        public IWebHostEnvironment _enviroment;
        public readonly IConfiguration _confg;
        public ProspectoController(IProspectoService _prospectoService, IWebHostEnvironment _enviroment, IConfiguration _confg) 
        {
            this._prospectoService = _prospectoService;
            this._enviroment = _enviroment;
            this._confg = _confg;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Prospecto>>> Get()
        {
            try
            {
                var prospectos = await this._prospectoService.GetProspectoAll();
                foreach (Prospecto p in prospectos) 
                {
                    p.documentos = await this._prospectoService.GetDocumentoAllByIdProspecto(p.id);
                    foreach (Documento d in p.documentos) 
                    {
                        d.documento = this._confg["Host:Enviroment"] + "Images/Documentos/" + d.documento;
                    }
                    if (p.estatus == 3) 
                    {
                        p.rechazo = await this._prospectoService.GetRechazoByIdProspecto(p.id);
                    }

                }
                return Ok(prospectos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Prospecto prosprecto)
        {
            try
            {
                int idProspecto = 0;
                Prospecto pros = prosprecto;
                idProspecto = await this._prospectoService.CreateProspecto(prosprecto);
                if (idProspecto != 0)
                {
                    foreach (Documento d in pros.documentos)
                    {

                        try
                        {
                            if (d.documento != "")
                            {
                                string photoName = Guid.NewGuid().ToString() + ".png";

                                SaveFileHelper saveFilesHelper = new SaveFileHelper(_enviroment);

                                bool result = await saveFilesHelper.SaveFile("\\Images\\Documentos\\", d.documento, photoName);

                                if (result)
                                {
                                    d.documento = photoName;
                                }
                                else
                                {
                                    d.documento = "";
                                }
                            }
                            d.idProspecto = idProspecto;
                            await this._prospectoService.CreateDocumento(d);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("autorizar")]
        public async Task<ActionResult> Autorizacion([FromBody] Prospecto prosprecto)
        {
            try
            {
                await this._prospectoService.AutorizacionProspecto(prosprecto.estatus, prosprecto.id);

                if (prosprecto.estatus == 3) 
                { 
                    await this._prospectoService.CreateMotivoRechazo(prosprecto.rechazo);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
