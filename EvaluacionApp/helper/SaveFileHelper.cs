﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EvaluacionApp.helper
{
    public class SaveFileHelper
    {
        public static IWebHostEnvironment _environment;
        public SaveFileHelper(IWebHostEnvironment enviroment)
        {
            _environment = enviroment;
        }
        public Task<bool> SaveFile(string path, string imageBase64, string imageName)
        {
            try
            {
                if (!Directory.Exists(_environment.WebRootPath + path))
                {
                    Directory.CreateDirectory(_environment.WebRootPath + path);
                }

                if (imageBase64.Contains("base64"))
                {
                    //imageBase64 = imageBase64.Substring(23, imageBase64.Length - 23);
                    string[] splitImage = imageBase64.Split(',');
                    imageBase64 = splitImage[1];
                }

                var bytes = Convert.FromBase64String(imageBase64);

                using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + path + imageName))
                {
                    fileStream.Write(bytes, 0, bytes.Length);
                    fileStream.Flush();
                }

                return Task.FromResult(true);
            }
            catch (Exception ex)
            {
                return Task.FromResult(false);
            }
        }
    }
}
