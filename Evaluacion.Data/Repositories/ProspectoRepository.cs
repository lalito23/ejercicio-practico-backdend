﻿using Evaluacion.Core.Models;
using Evaluacion.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Evaluacion.Data.Repositories
{
    public class ProspectoRepository : IProspectoRepository
    {
        public readonly IConfiguration _confg;

        public ProspectoRepository(IConfiguration _confg) 
        {
            this._confg = _confg;
        }

        public async Task AutorizacionProspecto(int estatus, int idprospecto)
        {
            MySqlConnection connection = null;
            string connectionStrings = _confg["connectionStrings:LocalConnectionsString"];
            await Task.Run(() =>
            {
                using (connection = new MySqlConnection(connectionStrings))
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand("sp_autorizacionProspecto", connection);
                    try
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new MySqlParameter("estatus", estatus));
                        command.Parameters.Add(new MySqlParameter("idProspecto", idprospecto));
                        command.ExecuteNonQuery();

                    }
                    catch (MySqlException ex)
                    {
                        if (connection != null)
                        {
                            connection.Close();
                        }
                    }
                }
            });
        }

        public async Task CreateDocumento(Documento documento)
        {
            MySqlConnection connection = null;
            string connectionStrings = _confg["connectionStrings:LocalConnectionsString"];
            await Task.Run(() =>
            {
                using (connection = new MySqlConnection(connectionStrings))
                {
                    try
                    {
                        connection.Open();
                        MySqlCommand command = new MySqlCommand("sp_createDocumento", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new MySqlParameter("nombre", documento.nombre));
                        command.Parameters.Add(new MySqlParameter("documento", documento.documento));
                        command.Parameters.Add(new MySqlParameter("idProspecto", documento.idProspecto));
                        command.ExecuteNonQuery();

                    }
                    catch (MySqlException ex)
                    {
                        if (connection != null)
                        {
                            connection.Close();
                        }
                    }
                }
            });
        }

        public async Task CreateMotivoRechazo(RechazoProspecto motivoRechazo)
        {
            MySqlConnection connection = null;
            string connectionStrings = _confg["connectionStrings:LocalConnectionsString"];
            await Task.Run(() =>
            {
                using (connection = new MySqlConnection(connectionStrings))
                {
                    connection.Open();
                    MySqlCommand command = new MySqlCommand("sp_createRechazoProspecto", connection);
                    try
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new MySqlParameter("motivo", motivoRechazo.motivo));
                        command.Parameters.Add(new MySqlParameter("idProspecto", motivoRechazo.idProspecto));
                        command.ExecuteNonQuery();

                    }
                    catch (MySqlException ex)
                    {
                        if (connection != null)
                        {
                            connection.Close();
                        }
                    }
                }
            });
        }

        public async Task<int> CreateProspecto(Prospecto prospecto)
        {
            MySqlConnection connection = null;
            string connectionStrings = _confg["connectionStrings:LocalConnectionsString"];
            return await Task.Run(() =>
            {
                using (connection = new MySqlConnection(connectionStrings))
                {
                    int idInsert = 0;
                    try
                    {
                        connection.Open();
                        MySqlCommand command = new MySqlCommand("sp_createProspecto", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new MySqlParameter("nombre", prospecto.nombre));
                        command.Parameters.Add(new MySqlParameter("primer_ap", prospecto.primer_ap));
                        command.Parameters.Add(new MySqlParameter("segundo_ap", prospecto.segundo_ap));
                        command.Parameters.Add(new MySqlParameter("rfc", prospecto.rfc));
                        command.Parameters.Add(new MySqlParameter("telefono", prospecto.telefono));
                        command.Parameters.Add(new MySqlParameter("calle", prospecto.calle));
                        command.Parameters.Add(new MySqlParameter("numero", prospecto.numero));
                        command.Parameters.Add(new MySqlParameter("colonia", prospecto.colonia));
                        command.Parameters.Add(new MySqlParameter("cp", prospecto.cp));
                        //command.ExecuteNonQuery();
                        MySqlDataAdapter data_adapter = new MySqlDataAdapter(command);
                        DataSet data_set = new DataSet();
                        data_adapter.Fill(data_set);
                        DataRow row = data_set.Tables[0].Rows[0];

                        idInsert = int.Parse(row[0].ToString());
                        return idInsert;

                    }
                    catch (MySqlException ex)
                    {
                        if (connection != null)
                        {
                            connection.Close();
                        }
                        return idInsert;
                    }
                }
            });
        }

        public async Task<IList<Documento>> GetDocumentoAllByIdProspecto(int idprospecto)
        {
            MySqlConnection connection = null;
            IList<Documento> objects = new List<Documento>();
            string connectionStrings = _confg["connectionStrings:LocalConnectionsString"];
            return await Task.Run(() =>
            {
                using (connection = new MySqlConnection(connectionStrings))
                {
                    try
                    {
                        connection.Open();
                        string query = "SELECT *  FROM documento WHERE idProspecto =" + idprospecto;
                        MySqlCommand command = new MySqlCommand(query, connection);
                        //command.CommandType = CommandType.StoredProcedure;
                        //command.Parameters.Add(new MySqlParameter("idProspecto", idprospecto));
                        MySqlDataAdapter data_adapter = new MySqlDataAdapter(command);
                        DataSet data_set = new DataSet();
                        data_adapter.FillAsync(data_set);
                        foreach (DataRow row in data_set.Tables[0].Rows)
                        {
                            objects.Add(new Documento
                            {
                                id = int.Parse(row[0].ToString()),
                                nombre = row[1].ToString(),
                                documento = row[2].ToString(),
                                idProspecto = int.Parse(row[3].ToString()),

                            });
                        }
                        return objects;

                    }
                    catch (MySqlException ex)
                    {
                        if (connection != null)
                        {
                            connection.Close();
                        }
                        return objects;
                    }
                }
            });
            
        }

        public async Task<IList<Prospecto>> GetProspectoAll()
        {
            MySqlConnection connection = null;
            IList<Prospecto> objects = new List<Prospecto>();
            string connectionStrings = _confg["connectionStrings:LocalConnectionsString"];
            return await Task.Run(() =>
            {
                using (connection = new MySqlConnection(connectionStrings))
                {
                    try
                    {
                        connection.Open();
                        MySqlCommand command = new MySqlCommand("sp_getAllProspecto", connection);
                        command.CommandType = CommandType.StoredProcedure;
                        MySqlDataAdapter data_adapter = new MySqlDataAdapter(command);
                        DataSet data_set = new DataSet();
                        data_adapter.Fill(data_set);
                        foreach (DataRow row in data_set.Tables[0].Rows)
                        {
                            objects.Add(new Prospecto
                            {
                                id = int.Parse(row[0].ToString()),
                                nombre = row[1].ToString(),
                                primer_ap = row[2].ToString(),
                                segundo_ap = row[3].ToString(),
                                calle = row[4].ToString(),
                                numero = row[5].ToString(),
                                colonia = row[6].ToString(),
                                cp = row[7].ToString(),
                                telefono = row[8].ToString(),
                                rfc = row[9].ToString(),
                                estatus = int.Parse(row[10].ToString()),

                            });
                        }
                        return  objects;

                    }
                    catch (MySqlException ex)
                    {
                        if (connection != null)
                        {
                            connection.Close();
                        }
                        return objects;
                    }
                }
            });
            
        }

        public async Task<RechazoProspecto> GetRechazoByIdProspecto(int idprospecto)
        {
            MySqlConnection connection = null;
            string connectionStrings = _confg["connectionStrings:LocalConnectionsString"];
            return await Task.Run(() =>
            {
                using (connection = new MySqlConnection(connectionStrings))
                {
                    try
                    {
                        connection.Open();
                        string query = "SELECT *  FROM rechazoprospecto WHERE idProspecto =" + idprospecto;
                        MySqlCommand command = new MySqlCommand(query, connection);
                        //command.CommandType = CommandType.StoredProcedure;
                        //command.Parameters.Add(new MySqlParameter("idProspecto", idprospecto));
                        MySqlDataAdapter data_adapter = new MySqlDataAdapter(command);
                        DataSet data_set = new DataSet();
                        data_adapter.FillAsync(data_set);
                        DataRow row = data_set.Tables[0].Rows[0];
                        return new RechazoProspecto
                        {
                            id = int.Parse(row[0].ToString()),
                            motivo = row[1].ToString(),
                            idProspecto = int.Parse(row[2].ToString()),

                        };

                    }
                    catch (MySqlException ex)
                    {
                        if (connection != null)
                        {
                            connection.Close();
                        }
                        return null;
                    }
                }
            });
        }
    }
}
