﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Evaluacion.Core.Models
{
    public class Prospecto
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string primer_ap { get; set; }
        public string segundo_ap { get; set; }
        public string rfc { get; set; }
        public string telefono { get; set; }
        public string calle { get; set; }
        public string numero { get; set; }
        public string colonia { get; set; }
        public string cp { get; set; }
        public int estatus { get; set; }
        public IList<Documento> documentos { get; set; }
        public RechazoProspecto rechazo { get; set; }
    }
}
