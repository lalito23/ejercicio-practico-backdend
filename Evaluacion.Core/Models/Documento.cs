﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Evaluacion.Core.Models
{
    public class Documento
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string documento { get; set; }
        public int idProspecto { get; set; }
    }
}
