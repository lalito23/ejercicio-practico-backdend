﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Evaluacion.Core.Models
{
    public class RechazoProspecto
    {
        public int id { get; set; }
        public string motivo { get; set; }
        public int idProspecto { get; set; }
    }
}
