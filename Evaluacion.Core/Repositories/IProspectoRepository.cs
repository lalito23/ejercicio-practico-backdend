﻿using Evaluacion.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Evaluacion.Core.Repositories
{
    public interface IProspectoRepository
    {
        Task<IList<Prospecto>> GetProspectoAll();
        Task<IList<Documento>> GetDocumentoAllByIdProspecto(int idprospecto);
        Task<int> CreateProspecto(Prospecto prospecto);
        Task CreateDocumento(Documento documento);
        Task CreateMotivoRechazo(RechazoProspecto motivoRechazo);
        Task AutorizacionProspecto(int estatus, int idprospecto);
        Task<RechazoProspecto> GetRechazoByIdProspecto(int idprospecto);

    }
}
