﻿using Evaluacion.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Evaluacion.Core.Services
{
    public interface IProspectoService
    {
        Task<IList<Prospecto>> GetProspectoAll();
        Task<IList<Documento>> GetDocumentoAllByIdProspecto(int idprospecto);
        Task<int> CreateProspecto(Prospecto prospecto);
        Task AutorizacionProspecto(int estatus, int idprospecto);
        Task CreateDocumento(Documento documento);
        Task CreateMotivoRechazo(RechazoProspecto motivoRechazo);
        Task<RechazoProspecto> GetRechazoByIdProspecto(int idprospecto);
    }
}
